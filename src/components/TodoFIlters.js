import React from "react";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

function TodoFilters({ todos, filterCompletedTodos , filterActive , filterAll }) {

    const [alignment, setAlignment] = React.useState('left');

  const handleAlignment = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  function handleActive() {
    filterActive()
  }

  function handleAll() {
    filterAll()
  }

  function handleCompleted() {
    filterCompletedTodos()
  }


  return (
    <ToggleButtonGroup
      size="small"
      value={alignment}
      exclusive
      onChange={handleAlignment}
      aria-label="text alignment"
    >
      <ToggleButton value="left" aria-label="left aligned" onClick={handleAll}>
          All
      </ToggleButton>
      <ToggleButton value="center" aria-label="left aligned" onClick={handleActive}>
          Active
      </ToggleButton>
      <ToggleButton value="rigth" aria-label="left aligned" onClick={handleCompleted}>
          Completed
      </ToggleButton>
    </ToggleButtonGroup>
  );
}

export default TodoFilters;
