import { TextField } from "@material-ui/core";
import React, { useState } from "react";
import uuid from "uuid";

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
      '& .MuiInput-underline:after': {
        borderBottomColor: 'black'
      },
  }
}));

function TodoForm({ addTodo }) {

  const classes = useStyles();


  const [todo, setTodo] = useState({
    id: "",
    task: "",
    completed: false
  });


  function handleTaskInputChange(e) {
    // e.target.value contains new input from onChange
    // event for input elements
    setTodo({ ...todo, task: e.target.value });
  }

  function handleSubmit(e) {
    e.preventDefault(); // prevents browser refresh
    // trim() gets rid of string whitespace
    if (todo.task.trim()) {
      addTodo({ ...todo, id: uuid.v4() });
      setTodo({ ...todo, task: "" });
    }
  }

  return (
    <form className="todo-form" onSubmit={handleSubmit}>
      <TextField
        className={classes.root}
        id="standard-full-width"
        placeholder="What needs to be done?"
        type="text"
        name="task"
        fullWidth
        value={todo.task}
        onChange={handleTaskInputChange}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </form>
  );
}


export default TodoForm;
