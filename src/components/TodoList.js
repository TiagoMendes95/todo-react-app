import { List } from "@material-ui/core";
import React from "react";
import Todo from "./Todo";

function TodoList({ todos, filter, filterMap, removeTodo, toggleComplete }) {
  return (
    <List style={{maxHeight: 190, overflow: 'auto'}} >
      {todos.filter(filterMap[filter]).map(todo => (
        <Todo
          key={todo.id}
          todo={todo}
          removeTodo={removeTodo}
          toggleComplete={toggleComplete}
        />
      ))}
    </List>
  );
}

export default TodoList;
