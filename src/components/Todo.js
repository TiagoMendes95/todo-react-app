import { Checkbox, 
  // IconButton, 
  ListItem, 
  Typography } from "@material-ui/core";
// import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';


function Todo({ todo, toggleComplete, removeTodo }) {
  function handleCheckboxClick() {
    toggleComplete(todo.id);
  }

  /* 
    ================================================= 
    Uncomment the code below to enable deleting todos
    =================================================
  */
  // function handleRemoveClick() {
  //   removeTodo(todo.id);
  // }


  return (
    <ListItem style={{ display: "flex" , height:45 }}>
      <Checkbox 
      style={{color:'#C9B79C'}}
      icon={<CircleUnchecked />}
      checkedIcon={<CircleCheckedFilled />} 
      checked={todo.completed} 
      onClick={handleCheckboxClick} />
      <Typography
        variant="body1"
        style={{
          textDecoration: todo.completed ? "line-through" : null
        }}
      >
        {todo.task}

      </Typography>
      {/* 
          ================================================= 
          Uncomment the code below to enable deleting todos
          =================================================
      */}

      {/* <IconButton onClick={handleRemoveClick}>
        <CloseIcon />
      </IconButton> */}
    </ListItem>
  );
}

export default Todo;
