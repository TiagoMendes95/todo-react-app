import Typography from "@material-ui/core/Typography";
import React, { useEffect, useState } from "react";
import "./App.css";
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";
import TodoFilters from "./components/TodoFIlters";

const LOCAL_STORAGE_KEY = "react-todo-list-todos";

function App() {
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState('All');

  const FILTER_MAP = {
    All: () => true,
    Active: task => !task.completed,
    Completed: task => task.completed
  };


  useEffect(() => {
    // fires when app component mounts to the DOM
    const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (storageTodos) {
      setTodos(storageTodos);
    }
  }, []);

  useEffect(() => {
    // fires when todos array gets updated
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
  }, [todos]);

  function addTodo(todo) {
    // adds new todo to beginning of todos array
    setTodos([todo, ...todos]);
  }

  function toggleComplete(id) {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          };
        }
        return todo;
      })
    );
  }

  function filterCompletedTodos() {
    setFilter("Completed");
  }

  function filterActive() {
    setFilter("Active");
  }

  function filterAll() {
    setFilter("All");
  }
  
  /*
    ================================================= 
    Uncomment the code below to enable deleting todos
    =================================================
  */
  // function removeTodo(id) {
  //   setTodos(todos.filter(todo => todo.id !== id));
  // }

  return (
    <div className="App">
      <Typography style={{ padding: 5 , "font-size" : 90  }} variant="h1">
        todos
      </Typography>
      <div className="content">
        <div className="header">
          <TodoForm addTodo={addTodo} />
        </div>
        <div className="list">
          <TodoList
            todos={todos}
            filter={filter}
            filterMap={FILTER_MAP}
            toggleComplete={toggleComplete}

            /* 
          ================================================= 
          Uncomment the code below to enable deleting todos
          =================================================
          */

          /* removeTodo={removeTodo} */
          />
        </div>
        <div className="footer">
          <div className="left-footer">
            <p>{todos.filter(task => !task.completed).length} items left</p>
          </div>
          <div className="rigth-footer" >
            <TodoFilters
            filterCompletedTodos={filterCompletedTodos}
            filterActive={filterActive}
            filterAll={filterAll}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
